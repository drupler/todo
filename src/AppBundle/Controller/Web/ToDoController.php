<?php
/**
 * Created by @drupler.
 */

namespace AppBundle\Controller\Web;

use AppBundle\Entity\ToDo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;


class ToDoController extends Controller {
  /**
   * @Route("/todo", name="todo")
   */
  public function showAction() {

    $templeting = $this->container->get('templating');
    $html = $templeting->render('todo/show.html.twig');

    return new Response($html);
  }
}