<?php
/**
 * Created by @drupler.
 */

namespace AppBundle\Controller\Api;

use AppBundle\Entity\ToDo;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\FOSRestBundle;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Tests\Util\Validator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class ToDoApiController extends FOSRestController {

  /**
   * @Route("/api/todo/new")
   * @Method("GET")
   */
  public function newAction(Request $request) {
    $data = json_decode($request->getContent(), TRUE);
    if (empty($data)) {
      throw new BadRequestHttpException("Content is empty");
    }

    $post = $this->createToDo($data);

    return new JsonResponse($post);
  }

  /**
   * @Rest\Get("/api/todos", name="todos")
   */
  public function listAction() {
    $em = $this->getDoctrine()->getManager();
    $stored_todos = $em->getRepository('AppBundle:ToDo')
      ->findAll();
    foreach ($stored_todos as $stored_todo) {
      $todos[] = array(
        'id' => $stored_todo->getId(),
        'title' => $stored_todo->getTitle(),
        'status' => $stored_todo->getStatus(),
        'created' => $stored_todo->getCreated(),
        'completed' => $stored_todo->getCompleted()
      );
    }

    return new JsonResponse($todos);
  }

  /**
   * @Rest\Get("/api/todo/{todo_id}", name="todo_show")
   */
  public function showAction($todo_id) {
    $em = $this->getDoctrine()->getManager();
    $query = $em->createQueryBuilder()
      ->select('td')
      ->from('AppBundle:ToDo', 'td')
      ->where('td.id = :id')
      ->setParameter('id', $todo_id)
      ->getQuery();

    $todo = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

    return new JsonResponse($todo);
  }

  /**
   * Method to create To Do.
   *
   * @param $data
   */
  public function createToDo($data) {
    $todo = new ToDo();
    $todo->setTitle($data['title']);
    $todo->setStatus($data['status']);
    $todo->setCreated(new \DateTime($data['created']));

    $em = $this->getDoctrine()->getManager();
    $em->persist($todo);
    $em->flush();
    $id = $todo->getId();

    return $id;
  }
}