AW ToDo
=======

### List of things done in this test project -- ###

* Route created for homepage(/todo).
* Attaching and rendering Twig template on homepage.
* Included base template in homepage template with default style.
* Added required libraries into base template such as Angular, Scrolling, Bootstrap.
* Used a simple bootstrap template for making page look better.
* Created database and entity for storing ToDo list. (DB configuration should be done in /app/config/parameters.yml)
* APIs for add, retrieve & list-all ToDo:
    * Add: [/api/todo/new](http://localhost:8000/api/todo/new)
        * POST method with JSON data. Format: {"title": "Some work", "status": 1, "created": "04/24/2017", "completed": "04/24/2017"}
        * Returns ID
    * Retrieve: [/api/todo/{ToDo_ID}](http://localhost:8000/api/todo/{ToDo_ID})
        * GET method with ToDo ID.
    * List All: [/api/todos](http://localhost:8000/api/todos)
    

### Resources ###
* PhpStorm Config - https://plugins.jetbrains.com/plugin/7219-symfony-plugin
* Routing and more docs - http://symfony.com/doc/current/components/routing.html
* Rest API - https://www.cloudways.com/blog/rest-api-in-symfony-3-1/
* Bootstrap Template - https://startbootstrap.com/template-overviews/scrolling-nav/
* AngularJs ToDo - https://github.com/tastejs/todomvc/tree/gh-pages/examples/angularjs
* Twig Verbatism - https://twig.sensiolabs.org/doc/2.x/tags/verbatim.html

